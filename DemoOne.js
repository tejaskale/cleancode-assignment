

// Avoid Magic Numbers. Instade of that create a constant and assign that number to it.
//eg.

// Case 1
if( password.length < 8 ){
    alert('Password too Short')
}

// Case 2

const min_password_length = 8;

if(password.length < min_password_length){
    alert('Password too Short')
}

//Here case 2 prefered over case 1

