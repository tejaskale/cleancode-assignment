// Avoide Useless Additional Context

// Case1
const employee={
    empName : 'John Snow',
    empAge :25,
    empSalary: 80000
}


// Case 2
const employee2={
    Name : 'John Snow',
    Age :25,
    Salary: 80000
}

// Here writing empName , empAge, empSalary again and again is useless, hence the case 2 prefered here.
